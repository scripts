##
## Put me in ~/.irssi/scripts, and then execute the following in irssi:
##
##       /load perl
##       /script load notifyudp
##

use strict;
use Irssi;
use IO::Socket;
use vars qw($VERSION %IRSSI);
use Time::HiRes qw(time);

$VERSION = "0.3.20140930";
%IRSSI = (
	authors     => 'Lekensteyn',
	contact     => 'lekensteyn@gmail.com',
	name        => 'notifyudp.pl',
	description => 'Send a UDP signal to a remote machine',
	license     => 'GPLv3+'
);

Irssi::settings_add_str('notifyudp', 'notifyudp_ip_addr', '');
# port 0 = disabled
Irssi::settings_add_int('notifyudp', 'notifyudp_port', 0);
Irssi::settings_add_bool('notifyudp', 'notifyudp_auto_start', 0);

my $sock;

sub notify_load {
	if ($sock) {
		Irssi::print('NotifyUDP: Already connected.');
		return;
	}
	my $ip = Irssi::settings_get_str('notifyudp_ip_addr');
	my $port = Irssi::settings_get_int('notifyudp_port');
	if (!$port || !$ip) {
		Irssi::print('NotifyUDP: No port or host set, /set notifyudp for more information..');
		return;
	}
	if ($port < 1024 || $port > 65535) {
		Irssi::print('NotifyUDP: Invalid port, must be 1024 <= port <= 65535, resetting and ignoring.');
		Irssi::settings_set_int('notifyudp_port', 0);
		return;
	}
	$sock = new IO::Socket::INET(
		PeerAddr => $ip,
		PeerPort => $port,
		Proto => 'udp',
		Timeout => 1
	);
	Irssi::print("NotifyUDP: IP $ip will be notified on port $port.");
}

my $last_time = 0;
sub notify {
	if ($sock) {
		my $now = time;
		my $notify_delay = 1.3;
		if (abs($now - $last_time) > $notify_delay) {
			$last_time = $now;
			$sock->send("M");
		}
	}
}
sub notify_if_hilighted {
	my ($dest, $text, $stripped) = @_;
	if ($dest->{level} & MSGLEVEL_HILIGHT) {
		notify();
	}
}

sub notify_stop {
	if ($sock) {
		Irssi::print("NotifyUDP: Stopping.");
		$sock->send("S");
		$sock = undef;
	} else {
		Irssi::print("NotifyUDP: not active.");
	}
}

sub cmd_notifyudp {
	my ($cmd) = @_;
	if ($cmd eq 'start') {
		notify_load();
	} elsif ($cmd eq 'stop') {
		notify_stop();
	} elsif ($cmd eq 'ping') {
		notify();
	} else {
		Irssi::print('NotifyUDP: Usage: /notifyudp [start|stop|ping]');
	}
}

Irssi::command_bind('notifyudp', 'cmd_notifyudp');

my @signals = (
# Uncomment the following to get notifs for every (channel) message
#'message public',
'message private',
'dcc request',

'message irc notice', # NickServ responses and such

# whenever the server dies
'server connected',
'server connect failed',
'server disconnected',

'message invite',
'message topic',
'message dcc',
'ctcp msg',
'ctcp reply',
);
Irssi::signal_add('print text', 'notify_if_hilighted');
foreach (@signals) {
	Irssi::signal_add($_, 'notify');
}

if (Irssi::settings_get_bool('notifyudp_auto_start')) {
	Irssi::print('NotifyUDP: automatic connection with the sound server is enabled.');
	notify_load();
} else {
	Irssi::print('NotifyUDP: automatic connection with the sound server is disabled.');
}
