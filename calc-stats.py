#!/usr/bin/python
# calculate statistics for a set of numbers from stdin

import sys
from math import sqrt

def mean(data):
	return sum(data) / len(data)

def variance(data):
	n = len(data)
	ss = sum(x**2 for x in data) - (sum(data)**2 / n)
	return ss / (n - 1)

def sdev(data):
	return sqrt(variance(data))

xs = []
for line in sys.stdin:
	xs.append(float(line))

print("Data:", xs)

print("n=", len(xs))
print("min:", min(xs))
print("max:", max(xs))
print("Mean:", mean(xs))
print("sd:", sdev(xs))
