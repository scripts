#!/bin/bash
url=$1
subject=$2
outfile=

case $url in
*/gitweb/*)
	url=$(sed -r 's/([;?])a=[^;]*/\1a=patch/' <<<"$url")
	;;
esac

if [ -z "$url" ]; then
	echo "Usage: $0 patch-url [subject]"
	exit
fi

normalize_name() {
	tr -c a-zA-Z0-9._ - | sed 's/--*/-/g;s/^[.-]*//;s/[.-]*$//' | cut -c1-50
}
find_subject() {
	awk '/^ /{if(s)s=s$0}
	{if(s)exit}
	/^Subject:/{s=s$0}
	END{sub(/^Subject: \[PATCH[^\]]*\] /,"",s);print s}'
}

subject=$(echo "$subject" | normalize_name)

if [ -n "$subject" ]; then
	outfile="$subject.patch"
	wget -O "$outfile" "$url" || exit 1
else
	outfile="$(mktemp patch-XXXXXXXXXX)"
	if wget -O "$outfile" "$url"; then
		subject=$(find_subject <"$outfile")
		if [ -z "$subject" ]; then
			echo "Subject not found!"
			rm "$outfile"
			exit 1
		fi

		patchfile="$(echo "$subject" | normalize_name).patch"
		mv "$outfile" "$patchfile"
		outfile="$patchfile"
	else
		rm "$outfile"
		exit 1
	fi
fi

echo "$outfile"
