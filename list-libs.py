#!/usr/bin/env python3
# For every given file, check its library dependencies, recursively.

import argparse
import re
import subprocess
import sys


def check_libs_macos(obj):
    p = subprocess.run(['otool', '-L', obj], stdout=subprocess.PIPE, text=True)
    print(p.stdout)
    try:
        p.check_returncode()
    except subprocess.CalledProcessError as e:
        print(str(e), file=sys.stderr)
    return re.findall(r'^\t(/\S+)', p.stdout, flags=re.M)


parser = argparse.ArgumentParser()
parser.add_argument('files', nargs='+',
                    help='Executable or library files to check')


def main():
    args = parser.parse_args()

    if sys.platform == 'darwin':
        check_libs = check_libs_macos
    elif sys.platform == 'linux':
        print('If you trust your program, find its dependencies with:')
        print('LD_DEBUG=files your-program --version 2>&1 | grep need')
        return
    else:
        raise RuntimeError('Unsupported platform')

    # Queue, top item is at the end.
    queue = args.files[::-1]
    seen = set()
    while queue:
        obj = queue.pop()
        if obj in seen:
            continue
        objs = check_libs(obj)
        queue.extend(objs[::-1])
        seen.add(obj)


if __name__ == '__main__':
    main()
