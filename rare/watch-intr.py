#!/usr/bin/env python
# Light-weight tool to watch the /proc/interrupts file, to be combined like:
#
#   ./watch-intr.py > intr.txt
#   ./interrupts-graph.py -i intr.txt

import argparse, sys, time

parser = argparse.ArgumentParser()
parser.add_argument("limit", type=int, default=0, nargs="?",
        help="Number of repetitions (or 0 for infinity), default %(default)d")
parser.add_argument("delay", type=int, default=1, nargs="?",
        help="Interval between printing, default %(default)d second")

args = parser.parse_args()
limit, delay = args.limit, args.delay

def print_and_flush(data):
    sys.stdout.write(data)
    sys.stdout.write("\n")
    sys.stdout.flush()

try:
    with open("/proc/interrupts") as f:
        print_and_flush(f.read())
        while limit != 1:
            if limit > 0:
                limit -= 1
            time.sleep(delay)
            f.seek(0)
            print_and_flush(f.read())
except KeyboardInterrupt:
    pass
